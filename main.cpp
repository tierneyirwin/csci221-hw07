#include "stdio.h"
#include <fstream>
#include <iostream>
#include <algorithm>
#include <vector>
#include <iterator>
#include "database.h"
#include "mp3.h"
#include <string>
#include "genres.h"
#include <istream>
#include <sstream>
#include "stdlib.h"
string genres[148];
using namespace std;
#include <boost/filesystem/operations.hpp>
#include <boost/filesystem/path.hpp>
using namespace boost::filesystem;

int main(){
	initialize_genres();
	Database* db = new Database("/home/tirwin/CSCI221/HW07/playlist");
	cout << "Read " << db->sizevect() << " MP3 files." << endl;
	string str;
	vector<string> vect;

		cout << "Type 'list' or enter a query: [artist/title/album/year] [query]" << endl;
		getline(cin,str);
		istringstream iss(str);
		copy(istream_iterator<string>(iss), istream_iterator<string>(), back_inserter(vect));
		if(vect.empty()){
			cout << "Huh?"<< endl;		
		}else if(vect[0] == "list"){
			db->show_All();
		}else if(vect[0] == "artist"){
			db->search_artist(vect[1]);
		}else if(vect[0] == "title"){
			db->search_title(vect[1]);
		}else if(vect[0] == "album"){
			db->search_album(vect[1]);
		}else if(vect[0] == "year"){
			db->search_year(vect[1]);
		}else{
			cout << "Huh?" << endl;	
		}

	delete db;
	return 0;

}
