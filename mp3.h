#ifndef MP3_H
#define MP3_H
#include <vector>
#include <string>
#include <boost/filesystem/operations.hpp>
#include <boost/filesystem/path.hpp>
using namespace boost::filesystem;
using namespace std;
class MP3 {
	public:
		MP3(path filepath);
		string getTitle();
		string getArtist();
		string getAlbum();
		string getYear();
		string getComment();
		string getGenre();
		string getFilepath();
	private:

		string filename;
		string title;
		string artist;
		string album;
		string year;
		string comment;
		string genre;
		string filepath;

};


#endif

