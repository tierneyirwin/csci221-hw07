#include "database.h"
#include "mp3.h"
#include <iostream>
#include <iterator>
#include <vector>
#include <string>
#include <cstring>
using namespace std;
#include <boost/filesystem/operations.hpp>
#include <boost/filesystem/path.hpp>
using namespace boost::filesystem;
Database::Database(string _path){//make string for path please
	//here create path into string
	read(_path);
}

void Database::read(string pdir){

	path p(pdir);
	for(directory_iterator it(p); it != directory_iterator(); ++it){
		path p2 = it->path();
		if(is_directory(p2)){
			read(p2.string()); 
		}else if(is_regular_file(p2)){
			string ext = p2.extension();
			if(ext == ".mp3"){
				addMP3(p2);
			}
		}
	}
}
void Database::show_All(){
	for(unsigned int i = 0; i < files.size(); i++){
		cout << files[i]->getArtist() << " - " << files[i]->getTitle() <<" (" <<  files[i]->getAlbum() << ", " << files[i]->getYear() << ") [" << files[i]->getComment() << "] " << files[i]->getGenre() << endl;
	}
}

void Database::search_title(string _title){

	for(unsigned int i = 0; i < files.size(); i++){
		string t = files[i]->getTitle();
		size_t found = t.find(_title);
		if(found!= string::npos){
			cout << files[i]->getArtist() << " - " << files[i]->getTitle() <<" (" <<  files[i]->getAlbum() << ", " << files[i]->getYear() << ") [" << files[i]->getComment() << "] " << files[i]->getGenre() << endl;

		}
	}

}

void Database::search_artist(string _artist){
	for(unsigned int i = 0; i < files.size(); i++){
		string t = files[i]->getArtist();
		size_t found = t.find(_artist);
		if(found!= string::npos){
			cout << files[i]->getArtist() << " - " << files[i]->getTitle() <<" (" <<  files[i]->getAlbum() << ", " << files[i]->getYear() << ") [" << files[i]->getComment() << "] " << files[i]->getGenre() << endl;

		}
	}
}

void Database::search_album(string _album){
	for(unsigned int i = 0; i < files.size(); i++){
		string t = files[i]->getAlbum();
		size_t found = t.find(_album);
		if(found!= string::npos){
			cout << files[i]->getArtist() << " - " << files[i]->getTitle() <<" (" <<  files[i]->getAlbum() << ", " << files[i]->getYear() << ") [" << files[i]->getComment() << "] " << files[i]->getGenre() << endl;

		}
	}
}

void Database::search_year(string _year){
	for(unsigned int i = 0; i < files.size(); i++){
		string t = files[i]->getYear();
		size_t found = t.find(_year);
		if(found!= string::npos){
			cout << files[i]->getArtist() << " - " << files[i]->getTitle() <<" (" <<  files[i]->getAlbum() << ", " << files[i]->getYear() << ") [" << files[i]->getComment() << "] " << files[i]->getGenre() << endl;

		}
	}
}

void Database::addMP3(path filename){
	MP3* myMP3 = new MP3(filename);
	files.push_back(myMP3);
}
int Database::sizevect(){
	return files.size();
}

Database::~Database(){
	for(unsigned int i = 0; i < files.size(); i++){
		delete files[i];
	}
}
