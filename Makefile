myprogram: main.o mp3.o database.o
	g++ -Wall -ansi -pedantic -g -O2 -o myprogram -lboost_filesystem -lboost_system main.o mp3.o database.o

main.o: main.cpp database.h
	g++ -Wall -ansi -pedantic -g -O2 -c main.cpp

database.o: database.cpp database.h mp3.h
	g++ -Wall -ansi -pedantic -g -O2 -c -lboost_filesystem -lboost_system database.cpp

mp3.o: mp3.cpp mp3.h
	g++ -Wall -ansi -pedantic -g -O2 -c mp3.cpp

all: myprogram

.PHONY: clean
clean:
	rm -f *.o myprogram

