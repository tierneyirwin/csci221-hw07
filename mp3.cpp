#include <string>
#include "mp3.h"
#include <cstdio>
#include <istream>
#include "stdio.h"
#include <fstream>
#include <cstring>
#include <cstdlib>
#include <ios>
#include <iostream>
#include <boost/filesystem/operations.hpp>
#include <boost/filesystem/path.hpp>
#include <boost/filesystem.hpp>
extern string genres[148];
using namespace boost::filesystem;
using namespace std;

MP3::MP3(path _filepath){
	//extern string genres[148];
	ifstream file(_filepath.string().c_str(), ios::in | ios::binary);	
	
	if(file.is_open()){
		file.seekg(file_size(_filepath)-128L);
		char pTag[4];
		file.read(pTag,3);
		pTag[3] = 0;
		string tag = string((const char*)pTag);
		if(tag == "TAG"){
			streampos var = file_size(_filepath) - 125L;
			file.seekg(var);
			char ptitle[31];
			file.read(ptitle, 30);
			ptitle[30] = 0;
			title = string((const char*)ptitle);

			file.seekg(var + 30L);
			char partist[31];
			file.read(partist, 30);
			partist[30] = 0;
			artist = string((const char*)partist);

			file.seekg(var + 60L);
			char palbum[31];
			file.read(palbum,30);
			palbum[30] = 0;
			album = string((const char*)palbum);

			file.seekg(var+90L);
			char pyear[5];
			file.read(pyear, 4);
			pyear[4] = 0;
			year = string((const char*)pyear);

			file.seekg(var+94L);
			char pcomment[31];
			file.read(pcomment,30);
			pcomment[30] = 0;
			comment = string((const char*)pcomment);

			file.seekg(var + 124L);
			char pgenre;
			file.read(&pgenre,1);
			if((int)pgenre < 0 || (int)pgenre > 147){
				genre = "Unknown";
			}else{
				genre = genres[(int)pgenre];
			}		 	
		}else{
//			cout << "nope" << endl;
		}
	}
//	cout << "here i am" << endl;
}

string MP3::getTitle(){
	return title;
}

string MP3::getArtist(){
	return artist;
}

string MP3::getAlbum(){
	return album;
}

string MP3::getYear(){
	return year;
}

string MP3::getComment(){
	return comment;
}

string MP3::getGenre(){
	return genre;
}

string MP3::getFilepath(){
	return filepath;
}






































