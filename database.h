#ifndef DATABASE_H
#define DATABASE_H
#include <vector>
#include "mp3.h"
#include <string>
#include <cstring>
using namespace std;
#include <boost/filesystem/operations.hpp>
#include <boost/filesystem/path.hpp>
using namespace boost::filesystem;

class Database {
	public:
		Database(string path); 
		void show_All();
		~Database();
		void search_title(string _title);
		void search_artist(string _artist);
		void search_album(string _album);
		void search_year(string _year);
		void addMP3(path filename);//or path
		int sizevect();
		void read(string file); //possible mypath, myfile
	private:
		vector<MP3*> files;		
	
};

#endif
